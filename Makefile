all: client server

client:
	g++ -Wall -o client  client.cpp -L/usr/lib -lssl -lcrypto -lpthread
server:
	g++ -Wall -o server server.cpp -L/usr/lib -lssl -lcrypto -lpthread
clean:
	rm -f client server
