#!/bin/bash
# ssl.sh which make rsa key, csr
if [ $# == 1 -a $1 == "clean" ]; then
    rm -f openssl.conf private.csr private.pem public.pem mycommoncrt.crt certificate.conf
    exit
fi
    
rm -f openssl.conf private.csr private.pem public.pem certificate.conf
openssl genrsa -out private.pem 2048
openssl rsa -in private.pem -pubout -out public.pem

cr=`echo $'\n.'`
cr=${cr%.}

read -p "Input contry code: [KR]$cr" contry
contry=${contry:-KR}
read -p "Input state name: [Seoul]$cr" state
state=${state:-Seoul}
read -p "Input locality name: [Seongbuk-gu]$cr" locality
locality=${locality:-Seongbuk-gu}
read -p "Input organization name: [SSAGUK]$cr" organization
organization=${organization:-SSAGUK}
read -p "Input organization unit name: [snb]$cr" organizationUnit
organizationUnit=${organizationUnit:-snb}
read -p "Input full domain name: [test.gilgil.net]$cr" domain
domain=${domain:-test.gilgil.net}
read -p "Input email: [bor1boridv@gmail.com]$cr" email
email=${email:-bor1boridv@gmail.com}
cat > certificate.conf << EOL
[req]
default_bits = 2048
prompt = no
default_md = sha256
req_extensions = req_ext
distinguished_name = dn

[ dn ]
C=$contry
ST=$state
L=$locality
O=$organization
OU=$organizationUnit
emailAddress=$email
CN = $domain

[ req_ext ]
subjectAltName = @alt_names

[ alt_names ]
IP.1 = 111.111.111.111
DNS.1 = test.com
EOL
openssl req -new -key private.pem -out private.csr -config certificate.conf
openssl req -x509 -days 365 -key private.pem -in private.csr -out mycommoncrt.crt -days 365 -config certificate.conf
