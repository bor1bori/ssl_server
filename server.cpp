#include <errno.h>
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <resolv.h>
#include "openssl/ssl.h"
#include "openssl/err.h"
#include <thread>
#include <iostream>
#include <list>
#include <mutex>
#include <vector>

using namespace std;

#define FAIL    -1
mutex mutex_lock;

// Create the SSL socket and intialize the socket address structure
int OpenListener(int port)
{
    int sd;
    struct sockaddr_in addr;
    sd = socket(PF_INET, SOCK_STREAM, 0);
    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = INADDR_ANY;
    if (bind(sd, (struct sockaddr*)&addr, sizeof(addr)) != 0 )
    {
        perror("can't bind port");
        abort();
    }
    if ( listen(sd, 10) != 0 )
    {
        perror("Can't configure listening port");
        abort();
    }
    return sd;
}
int isRoot()
{
    if (getuid() != 0)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}
SSL_CTX* InitServerCTX(void)
{
    const SSL_METHOD *method;
    SSL_CTX *ctx;
    OpenSSL_add_all_algorithms();  /* load & register all cryptos, etc. */
    SSL_load_error_strings();   /* load all error messages */
    method = TLSv1_2_server_method();  /* create new server-method instance */
    ctx = SSL_CTX_new(method);   /* create new context from method */
    if ( ctx == NULL )
    {
        ERR_print_errors_fp(stderr);
        abort();
    }
    return ctx;
}
void LoadCertificates(SSL_CTX* ctx, char* CertFile, char* KeyFile)
{
    /* set the local certificate from CertFile */
    if ( SSL_CTX_use_certificate_file(ctx, CertFile, SSL_FILETYPE_PEM) <= 0 )
    {
        ERR_print_errors_fp(stderr);
        abort();
    }
    /* set the private key from KeyFile (may be the same as CertFile) */
    if ( SSL_CTX_use_PrivateKey_file(ctx, KeyFile, SSL_FILETYPE_PEM) <= 0 )
    {
        ERR_print_errors_fp(stderr);
        abort();
    }
    /* verify private key */
    if ( !SSL_CTX_check_private_key(ctx) )
    {
        fprintf(stderr, "Private key does not match the public certificate\n");
        abort();
    }
}
void ShowCerts(SSL* ssl)
{
    X509 *cert;
    char *line;
    cert = SSL_get_peer_certificate(ssl); /* Get certificates (if available) */
    if ( cert != NULL )
    {
        printf("Server certificates:\n");
        line = X509_NAME_oneline(X509_get_subject_name(cert), 0, 0);
        printf("Subject: %s\n", line);
        free(line);
        line = X509_NAME_oneline(X509_get_issuer_name(cert), 0, 0);
        printf("Issuer: %s\n", line);
        free(line);
        X509_free(cert);
    }
    else
        printf("No certificates.\n");
}
void send_broadcast(list<SSL*> *ssls, char* buf) {
    mutex_lock.lock();
    for (list<SSL*>::iterator it = ssls->begin(); it != ssls->end() ; it ++) {
        int sent = SSL_write((*it), buf, strlen(buf)); /* send reply */
		if (sent <= 0) {
			ssls->remove((*it));
			perror("send failed");
		}
	}
    mutex_lock.unlock();
}
void Servlet(SSL* ssl, list<SSL*>* ssls, int flab_b) /* Serve the connection -- threadable */
{
    char buf[1024] = {0};
    int sd, bytes;
    int accepted = SSL_accept(ssl);
    if (!accepted) {     /* do SSL-protocol accept */
        ERR_print_errors_fp(stderr);
    }
    while (true && accepted) {
        ShowCerts(ssl);        /* get any certificates */
        bytes = SSL_read(ssl, buf, sizeof(buf)); /* get request */
        if (bytes <= 0) {
            ERR_print_errors_fp(stderr);
            break;
        }
        if (bytes < 0) {
            ERR_print_errors_fp(stderr);
            break;
        }
        buf[bytes] = '\0';
        printf("Client msg: \"%s\"\n", buf);

        if (flab_b) {
            send_broadcast(ssls, buf);
        } else {
            SSL_write(ssl, buf, strlen(buf)); /* send reply */
        }
    }
    sd = SSL_get_fd(ssl);       /* get socket connection */
    SSL_free(ssl);         /* release SSL state */
    close(sd);          /* close connection */
}
void usage(char * filename) {
    printf("Usage: %s <portnum> [-b]\n", filename);
}
int main(int argc, char *argv[])
{
	list<SSL*> ssls;
    SSL_CTX *ctx;
    int server;
    int b_flag = 0;
    char *portnum;
//Only root user have the permsion to run the server
    if(!isRoot())
    {
        printf("This program must be run as root/sudo user!!");
        exit(0);
    }
    if ( !(argc == 2 || argc == 3) ) {
        usage(argv[0]);
        exit(0);
    }
    if (argc == 3) {
		if (strncmp(argv[2], "-b", 2)) {
            usage(argv[0]);
            exit(0);
		} else {
			b_flag = 1;
		}
	}
    // Initialize the SSL library
    SSL_library_init();
    portnum = argv[1];
    ctx = InitServerCTX();        /* initialize SSL */
    LoadCertificates(ctx, "mycommoncrt.crt", "private.pem"); /* load certs */
    server = OpenListener(atoi(portnum));    /* create server socket */
    while (1)
    {
        struct sockaddr_in addr;
        socklen_t len = sizeof(addr);
        SSL *ssl;
        int client = accept(server, (struct sockaddr*)&addr, &len);  /* accept connection as usual */

        printf("Connection: %s:%d\n",inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));
        ssl = SSL_new(ctx);              /* get new SSL state with context */
        SSL_set_fd(ssl, client);      /* set connection socket to SSL state */

        mutex_lock.lock();
        ssls.push_back(ssl);
        mutex_lock.unlock();

		thread t(&Servlet, ssl, &ssls, b_flag);//, &fds, b_flag);
        t.detach();
    }

    close(server);          /* close server socket */
    SSL_CTX_free(ctx);         /* release context */
}